<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use App\Models\Biodata;
use Exception;

class BioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $biodata = Biodata::query();

            $pagination = 2;
            $biodata = $biodata->orderBy('created_at','desc') ->paginate($pagination);

            $response = $biodata;
            $code = 200;

        } catch (Exception $te) {
            $code = 500;
            $response = $e ->getMessage();
        }
       return apiResponseBuilder($code,$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            'name' => 'required',
            'gender' => 'required',
            'Date_of_birth' => 'required',
            'age' => 'required',
            'father_name' => 'required',
            'mother_name' => 'required',
            'address' => 'required',
            'phone_number' => 'required',
            'religion' => 'required'
        ]);

         try {
            $biodata = new Biodata();

            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);

            $biodata->name = $request->name;
            $biodata->gender = $request->gender;
            $biodata->Date_of_birth = $request->Date_of_birth;
            $biodata->age = $request->age;
            $biodata->father_name = $request->father_name;
            $biodata->mother_name = $request->mother_name;
            $biodata->address = $request->address;
            $biodata->phone_number = $request->phone_number;
            $biodata->religion = $request->religion;

            $biodata->save();
            $code = 200;
            $response = $biodata;

        } catch (Exception $e) {
            if ($e instanceof ValidationException){
                $code = 400;
                $response = 'tidak ada data';
            }else{
                $code = 500;
                $response=$e->getMessage();
            }
        }
        return apiResponseBuilder ($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $biodata = Biodata::findOrFail($id);

            $code = 200;
            $response = $biodata;
        } catch (Exception $e) {
            if($e instanceof ModelNotFoundException){
                $code = 404;
                $response = 'iputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }
        return apiResponseBuilder ($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
             $this->validate($request,[
            'name' => 'required',
            'gender' => 'required',
            'Date_of_birth' => 'required',
            'age' => 'required',
            'father_name' => 'required',
            'mother_name' => 'required',
            'address' => 'required',
            'phone_number' => 'required',
            'religion' => 'required'
        ]);

        try {
            $biodata = new Biodata();

            $biodata->name = $request->name;
            $biodata->gender = $request->gender;
            $biodata->Date_of_birth = $request->Date_of_birth;
            $biodata->age = $request->age;
            $biodata->father_name = $request->father_name;
            $biodata->mother_name = $request->mother_name;
            $biodata->address = $request->address;
            $biodata->phone_number = $request->phone_number;
            $biodata->religion = $request->religion;

            $biodata->save();
            $code = 200;
            $response = $biodata;

    } catch (Exception $e) {
            if ($e instanceof ValidationException){
                $code = 400;
                $response = 'tidak ada data';
            }else{
                $code = 500;
                $response=$e->getMessage();
            }
        }
        return apiResponseBuilder ($code,$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         try {
            $biodata = Biodata::find($id);
            $biodata->delete();

            $code = 200;
            $response = $biodata;
        } catch (Exception $e) {
            $code = 500;
            $response = $e->getMessage();
        }
        return apiResponseBuilder ($code,$response);
    }
}
