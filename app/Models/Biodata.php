<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    use SoftDeletes;

    protected $table = "biodata" ;
}
