<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('bio','API\V1\BioController');


// Route::post('/biostudent','API\V1\BioController@store');
// Route::get('/biostudent','API\V1\BioController@index');
// Route::get('/biostudent/{id}','API\V1\BioController@show');
// Route::put('/biostudent/{id}/update','API\V1\BioController@update');
// Route::get('/biostudent/{id}/delete','API\V1\BioController@delete');

